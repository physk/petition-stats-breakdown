<?php

error_reporting( E_ALL );
set_error_handler("handleError");
function handleError($errno, $errstr,$error_file,$error_line) {
    echo "<b>Error:</b> [$errno] $errstr - $error_file:$error_line";
    echo "<br />";
    echo "Terminating PHP Script";
    
    die();
}
function bd_nice_number($n) {
    // first strip any formatting;
    $n = (0+str_replace(",","",$n));
    
    // is this a number?
    if(!is_numeric($n)) return false;
    
    // now filter it;
    if($n>1000000000000) return round(($n/1000000000000),1).' trillion';
    else if($n>1000000000) return round(($n/1000000000),1).' billion';
    else if($n>1000000) return round(($n/1000000),1).' million';
    else if($n>1000) return round(($n/1000),1).' thousand';
    
    return number_format($n);
}
if(isset($_GET["petitionid"]) && is_numeric($_GET["petitionid"])){
    $petitionid = $_GET["petitionid"];
}
else {
    $petitionid = 241584;
}
$contents = json_decode(file_get_contents("https://petition.parliament.uk/petitions/".$petitionid.".json"), true);

echo "<html>".PHP_EOL;
echo "<head>".PHP_EOL;
echo "<title>".PHP_EOL;
echo $contents["data"]["attributes"]["action"] . PHP_EOL;
echo "</title>".PHP_EOL;
echo "<style type=\"text/css\">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-0lax{text-align:left;vertical-align:top}
.text-align {text-align:right !important;}
</style>";
echo "</head>".PHP_EOL;
echo "<body>".PHP_EOL;
$total_count = 0;
$non_uk_count = 0;
$html_by_country = "";
$html_by_country .= "<h2>Signatures By Country</h2><br />".PHP_EOL;
$html_by_country .= "<table class=\"tg\">".PHP_EOL;
$html_by_country .= "  <tr>".PHP_EOL;
$html_by_country .= "    <th class=\"tg-0lax\"><b>Country</b></th>".PHP_EOL;
$html_by_country .= "    <th class=\"tg-0lax\"><b>Amount of Signatures</b></th>".PHP_EOL;
$html_by_country .= " </tr>".PHP_EOL;
foreach($contents["data"]["attributes"]["signatures_by_country"] as $result) {
    $html_by_country .= "  <tr>".PHP_EOL;
    $html_by_country .= "    <td class=\"tg-0lax\">".$result["name"]."</td>".PHP_EOL;
    $html_by_country .= "    <td class=\"tg-0lax\">".$result["signature_count"]."</td>".PHP_EOL;
    $html_by_country .= "  </tr>".PHP_EOL;
    $total_count = $total_count + $result["signature_count"];
    if($result["code"] == "GB") {
        $uk_count = $result["signature_count"];
    }
    else {
        $non_uk_count = $non_uk_count + $result["signature_count"];
    }
}
$html_by_country .= "</table>".PHP_EOL;

$html_by_constituency = "";
$html_by_constituency .= "<h2>Signatures By Constituency</h2><br />".PHP_EOL;
$html_by_constituency .= "<table class=\"tg\">".PHP_EOL;
$html_by_constituency .= "  <tr>".PHP_EOL;
$html_by_constituency .= "    <th class=\"tg-0lax\"><b>Constituency</b></th>".PHP_EOL;
$html_by_constituency .= "    <th class=\"tg-0lax\"><b>Amount of Signatures</b></th>".PHP_EOL;
$html_by_constituency .= " </tr>".PHP_EOL;
foreach($contents["data"]["attributes"]["signatures_by_constituency"] as $result) {
    $html_by_constituency .= "  <tr>".PHP_EOL;
    $html_by_constituency .= "    <td class=\"tg-0lax\">".$result["name"]."</td>".PHP_EOL;
    $html_by_constituency .= "    <td class=\"tg-0lax\">".$result["signature_count"]."</td>".PHP_EOL;
    $html_by_constituency .= "  </tr>".PHP_EOL;
}
$html_by_constituency .= "</table>".PHP_EOL;

#http://www.worldometers.info/world-population/uk-population/
$uk_population = 66869455;
$uk_population_date = "Sunday, April 7, 2019";

$perc_of_uk_population = ($uk_count / $uk_population) * 100;
$perc_non_uk = ($non_uk_count / $total_count) * 100;

echo "<h1>" . $contents["data"]["attributes"]["action"] . "</h1><br />" . PHP_EOL;
echo "Official data from: <a href=\"" . $contents["links"]["self"] . "\">". $contents["links"]["self"] ."</a><br />" . PHP_EOL;
echo "Petition page: <a href=\"https://petition.parliament.uk/petitions/". $contents["data"]["id"]."\">https://petition.parliament.uk/petitions/".$contents["data"]["id"]."</a><br />".PHP_EOL;
echo "<h2>Background</h2>".PHP_EOL;
echo $contents["data"]["attributes"]["background"] ."<br /><br />". PHP_EOL;
if ($contents["data"]["attributes"]["government_response"] !== null) {
    echo "<h2>Government Response</h2>".PHP_EOL;
    echo "<h3>Date: " . $contents["data"]["attributes"]["government_response"]["responded_on"] . "</h3>";
    echo $contents["data"]["attributes"]["government_response"]["details"];
}
if ($contents["data"]["attributes"]["debate"] !== null) {
    echo "<h2>Debate</h2>".PHP_EOL;
    echo "<h3>Date: " . $contents["data"]["attributes"]["debate"]["debated_on"] . "</h3>";
    preg_match('/v=([A-Za-z0-9]+)/', $contents["data"]["attributes"]["debate"]["video_url"], $output_array);
    echo "<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/".$output_array[1]."\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe><br />".PHP_EOL;
    echo "Transcript URL: <a href=\"".$contents["data"]["attributes"]["debate"]["transcript_url"]."\">".$contents["data"]["attributes"]["debate"]["transcript_url"]."</a>".PHP_EOL;
}
echo "<h2>Stats</h2>" . PHP_EOL;
echo "<table class=\"tg\">" . PHP_EOL;
echo "  <tr>" . PHP_EOL;
echo "    <td class=\"tg-0pky\"><b>Total Signature Count</b></td>" . PHP_EOL;
echo "    <td class=\"tg-0lax text-align\">". number_format($total_count) . "</td>" . PHP_EOL;
echo "    <td class=\"tg-0pky text-align\">".bd_nice_number($total_count)."</td>" . PHP_EOL;
echo "  </tr>" . PHP_EOL;
echo "  <tr>" . PHP_EOL;
echo "    <td class=\"tg-0pky\"><b>Total UK Signature Count</b></td>" . PHP_EOL;
echo "    <td class=\"tg-0lax text-align\">". number_format($uk_count) . "</td>" . PHP_EOL;
echo "    <td class=\"tg-0pky text-align\">".bd_nice_number($uk_count)."</td>" . PHP_EOL;
echo "  </tr>" . PHP_EOL;
echo "  <tr>" . PHP_EOL;
echo "    <td class=\"tg-0pky\"><b>Total UK Population (as of ".$uk_population_date.")<br/> based on the United Nations estimates</b></td>" . PHP_EOL;
echo "    <td class=\"tg-0lax text-align\">". number_format($uk_population) . "</td>" . PHP_EOL;
echo "    <td class=\"tg-0pky text-align\">".bd_nice_number($uk_population)."</td>" . PHP_EOL;
echo "  </tr>" . PHP_EOL;
echo "  <tr>" . PHP_EOL;
echo "    <td class=\"tg-0pky\"><b>Percentage of UK Population Signed</b></td>" . PHP_EOL;
echo "    <td class=\"tg-0pky text-align\" colspan=\"2\">".round($perc_of_uk_population, 2)."%</td>" . PHP_EOL;
echo "  </tr>" . PHP_EOL;
echo "  <tr>" . PHP_EOL;
echo "    <td class=\"tg-0pky\"><b>Total NON-UK Resident Signature Count</b></td>" . PHP_EOL;
echo "    <td class=\"tg-0lax text-align\">" . number_format($non_uk_count) . "</td>" . PHP_EOL;
echo "    <td class=\"tg-0pky text-align\">".bd_nice_number($non_uk_count)."</td>" . PHP_EOL;
echo "  </tr>" . PHP_EOL;
echo "  <tr>" . PHP_EOL;
echo "    <td class=\"tg-0pky\"><b>Percentage NON-UK Residents</b></td>" . PHP_EOL;
echo "    <td class=\"tg-0pky text-align\" colspan=\"2\">".round($perc_non_uk, 2)."%</td>" . PHP_EOL;
echo "  </tr>" . PHP_EOL;
echo "</table>" . PHP_EOL;
echo "<b>**Please allow a percentage for people that are using a VPN to fool the petition system into thinking they are from the UK**</b><br />".PHP_EOL;
echo $html_by_country;
echo $html_by_constituency;
echo "</body>".PHP_EOL;
echo "</html>".PHP_EOL;